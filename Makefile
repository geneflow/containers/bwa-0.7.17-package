# This downloads and builds bwa 0.7.17 statically.
# You may need to install some build tool dependencies for static compilation:

# glibc-static
# zlib-static
# zlib-devel

all: ./install/bin/bwa

./build/bwa-0.7.17/bwa:
	mkdir -p ./build
	wget -P ./build https://github.com/lh3/bwa/archive/v0.7.17.tar.gz
	tar -xzf ./build/v0.7.17.tar.gz -C ./build
	sed -i '/^CFLAGS/ s/$$/ -static/' ./build/bwa-0.7.17/Makefile
	make -C ./build/bwa-0.7.17

./install/bin/bwa: ./build/bwa-0.7.17/bwa
	mkdir -p ./install/bin
	cp ./build/bwa-0.7.17/bwa ./install/bin

clean: FORCE
	rm -rf ./build
	rm -rf ./install

FORCE:

